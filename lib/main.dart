import 'package:flutter/material.dart';

void main () => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String vietnameseGreeting = "Xin chào Flutter";
String frenchGreeting = "Bonjour Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {

  String displayText = englishGreeting;
  int _selectedIndex = 0;

  static const TextStyle optionStyle = TextStyle(fontSize: 24);

  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'สวัสดี Flutter',
      style: optionStyle,
    ),
    Text(
      'Xin chào Flutter',
      style: optionStyle,
    ),
    Text(
      'Bonjour Flutter',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting ?
                    spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh))
          ],
        ),
          body: Center(
            child: _widgetOptions.elementAt(_selectedIndex),
            // child: Text(
            //   displayText,
            //   style: TextStyle(fontSize: 24),
            // ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.temple_buddhist),
                label: 'Thai',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.star),
                label: 'Vietnamese',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.language),
                label: 'French',
              ),
            ],

            currentIndex: _selectedIndex,
            selectedItemColor: Colors.redAccent,
            onTap: _onItemTapped,

          ),
        ),
    );
  }
}




// // SafeArea Widjet
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//         home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }
